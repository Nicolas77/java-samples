package rendezvous;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ConnectionManagerTest {

    @Test
    public void testConnectionOK() throws InterruptedException {

        int max = 10;
        ExecutorService executorService = Executors.newFixedThreadPool(max);

        for(int i = 0; i < max; i++) {
            executorService.submit(connectionOKtoLocalhost8080());
            executorService.submit(connectionOKtoLocalhost8081());
        }
        executorService.shutdown();

        executorService.awaitTermination(20, TimeUnit.SECONDS);


    }


    private Runnable connectionOKtoLocalhost8081() {
        return () -> {
            String res = null;
            try {
                res = ConnectionManager.INSTANCE.getConnection("localhost",8081);
                Assert.assertEquals("OK", res);
            } catch (InterruptedException e) {
                Assert.fail();
            }
        };
    }

    private Runnable connectionOKtoLocalhost8080() {
        return () -> {
            String res = null;
            try {
                res = ConnectionManager.INSTANCE.getConnection("localhost",8080);
                Assert.assertEquals("OK", res);
            } catch (InterruptedException e) {
                Assert.fail();
            }
        };
    }

}
