package rendezvous;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class ConnectionManager {

    private final ConcurrentHashMap<Key, Connection> connections;


    public final static ConnectionManager INSTANCE = new ConnectionManager();

    private ConnectionManager() {
        connections = new ConcurrentHashMap();
    }

    public String getConnection(String host, int port) throws InterruptedException {

        Key key = new Key(host, port);
        Connection connection = connections.get(key);

        if(connection == null) {

            synchronized(this) {
                if(connections.get(key) == null) {
                    connections.putIfAbsent(key, new Connection());
                    System.out.println("Create a new connection object");
                }
            }

            connection = connections.get(key);
            connection.connect();

        }

        connection.await(10, TimeUnit.SECONDS);
        return connection.getRes();
    }



    private class Key {

        private final String host;
        private final int port;

        public Key(String host, int port) {
            this.host = host;
            this.port = port;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Key key = (Key) o;

            if (port != key.port) return false;
            return host != null ? host.equals(key.host) : key.host == null;
        }

        @Override
        public int hashCode() {
            int result = host != null ? host.hashCode() : 0;
            result = 31 * result + port;
            return result;
        }
    }

}
