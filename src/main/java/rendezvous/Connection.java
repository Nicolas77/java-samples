package rendezvous;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class Connection {

    private CountDownLatch countDownLatch = new CountDownLatch(1);

    private String res = null;

    private AtomicBoolean isRunning = new AtomicBoolean(false);

    public CountDownLatch getCountDownLatch() {
        return countDownLatch;
    }

    public void await() throws InterruptedException {
        this.countDownLatch.await();
    }

    public void await(long timeout, TimeUnit unit) throws InterruptedException {
        this.countDownLatch.await(timeout, unit);
    }

    public String getRes() {
        return res;
    }

    public void connect() {
        if(isRunning.compareAndSet(false, true)) {
            new Thread(() -> {

                try {
                    System.out.println("run " + Thread.currentThread().getName());
                    Thread.sleep(5_000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                res = "OK";
                countDownLatch.countDown();
            }).start();
        }

    }



}
