package mailbox;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Setup {


    public static void main(String[] args) throws InterruptedException {


        ExecutorService executorService = Executors.newFixedThreadPool(10);

        executorService.submit(Setup.askConnection("key1"));
        executorService.submit(Setup.askConnection("key1"));
        executorService.submit(Setup.askConnection("key2"));
        executorService.submit(Setup.askConnection("key2"));
        executorService.submit(Setup.askConnection("key3"));
        executorService.submit(Setup.askConnection("key3"));

        executorService.shutdown();

        executorService.awaitTermination(10, TimeUnit.SECONDS);



    }

    public static Runnable askConnection(String key) {

        return new Runnable() {
            @Override
            public void run() {
                System.out.println("ask " + key);
                ConnectionPooled connectionPooled = ConnectionServicePool.INSTANCE.get(key);
                System.out.println(key + " received " + connectionPooled.getStatus());
            }
        };


    }


}
