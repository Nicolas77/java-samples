package mailbox;

import java.util.concurrent.CountDownLatch;

public class MessageAskConnection extends Message{

    private CountDownLatch countDownLatch = new CountDownLatch(1);

    private ConnectionPooled connection;

    public MessageAskConnection(String key) {
        super(key);
    }

    public void awaitConnectionResponse() {
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public ConnectionPooled getConnection() {
        return connection;
    }

    public void notifyResponseAvailable(ConnectionPooled connection) {
        this.connection = connection;
        this.countDownLatch.countDown();
    }

}
