package mailbox;

public class MessageReturnConnection extends Message{

    private FakeConnection connection;

    public MessageReturnConnection(String key, FakeConnection connection) {
        super(key);
        this.connection = connection;
    }

    public FakeConnection getConnection() {
        return connection;
    }
}
