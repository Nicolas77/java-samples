package mailbox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConnectionServicePool implements ConnectionService {

    public final static ConnectionServicePool INSTANCE = new ConnectionServicePool();

    private Thread consumer;

    private BlockingQueue<Message> queue;

    private Map<String, ConnectionPooled> cache;

    private Map<String, List<MessageAskConnection>> mapOfConnectionAsk;

    private ConnectionServicePool() {
        queue = new ArrayBlockingQueue(1000);
        cache = new HashMap<>();
        mapOfConnectionAsk = new HashMap<>();
        consumer = createConsumer();
        consumer.setDaemon(true);
        consumer.start();
    }

    private Thread createConsumer() {

        return new Thread() {
            @Override
            public void run() {

                while(true) {

                    try {
                        Message message = queue.take();

                        String key = message.getKey();
                        ConnectionPooled connection = cache.get(key);
                        if(message instanceof MessageAskConnection) {
                            MessageAskConnection messageAskConnection = (MessageAskConnection) message;
                            if(connection == null) {
                                connection = new ConnectionPooled();
                                cache.put(key, connection);
                                List<MessageAskConnection> list = mapOfConnectionAsk.get(key);
                                if(list == null) {
                                    list = new ArrayList<>();
                                    mapOfConnectionAsk.put(key, list);
                                }
                                list.add(messageAskConnection);

                                ConnectionThread connectionThread = new ConnectionThread(key, queue);
                                connectionThread.start();

                            }

                            switch (connection.getStatus()) {
                                case CONNECTED:
                                case NOT_CONNECTED:
                                    messageAskConnection.notifyResponseAvailable(connection);
                                    break;
                                case CONNECTING:
                                    List<MessageAskConnection> list = mapOfConnectionAsk.get(key);
                                    list.add(messageAskConnection);
                            }
                        } else if (message instanceof MessageReturnConnection) {
                            MessageReturnConnection messageReturnConnection = (MessageReturnConnection) message;

                            FakeConnection fakeConnection = messageReturnConnection.getConnection();
                            if(fakeConnection == null) {
                                connection.setStatus(ConnectionPooled.Status.NOT_CONNECTED);
                            } else {
                                connection.setStatus(ConnectionPooled.Status.CONNECTED);
                                connection.setConnection(fakeConnection);
                            }

                            List<MessageAskConnection> list = mapOfConnectionAsk.get(key);
                            for(MessageAskConnection messageAskConnection : list) {
                                messageAskConnection.notifyResponseAvailable(connection);
                            }
                            list.clear();

                        }





                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            }
        };

    }


    @Override
    public ConnectionPooled get(String key) {
        MessageAskConnection message = new MessageAskConnection(key);
        queue.add(message);
        message.awaitConnectionResponse();
        return message.getConnection();
    }




    private class ConnectionThread extends Thread{

        private BlockingQueue<Message> queue;

        private String key;

        public ConnectionThread(String key, BlockingQueue<Message> queue) {
            super("connection-on-" + key);
            this.key = key;
            this.queue = queue;
        }

        @Override
        public void run() {

            FakeConnection connection = null;
            try {
                if("key3".equals(key)) {
                    Thread.sleep(2_000);
                    throw new InterruptedException();
                }
                Thread.sleep(5_000);
                connection = new FakeConnection();
            } catch (InterruptedException e) {
                System.out.println("Connection problem to " + key);
            } finally {
                queue.add(new MessageReturnConnection(key, connection));
            }
        }
    }


}
