package mailbox;

public abstract class Message {

    private String key;

    public Message(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
