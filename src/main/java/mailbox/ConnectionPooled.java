package mailbox;

public class ConnectionPooled {

    public enum Status {
        CONNECTING,
        NOT_CONNECTED,
        CONNECTED
    }

    private Status status;

    private FakeConnection connection;

    public ConnectionPooled() {
        status = Status.CONNECTING;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public FakeConnection getConnection() {
        return connection;
    }

    public void setConnection(FakeConnection connection) {
        this.connection = connection;
    }
}
