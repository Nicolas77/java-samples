package proxy;

public class TestProxy {

    public static void main(String[] args) {
        Customer myCustomer = new CustomerIdZero();
        Customer instance = (Customer) CustomHandler.newInstance(myCustomer);

        for(int i=0;i<10;i++) {
            System.out.println(instance.getRand());
        }
    }

}
