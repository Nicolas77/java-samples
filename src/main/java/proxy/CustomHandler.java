package proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Random;

public class CustomHandler implements InvocationHandler {

    private volatile boolean change;

    private Customer obj;

    public static Object newInstance(Customer obj) {
        return java.lang.reflect.Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), new CustomHandler(
                obj));
    }

    private CustomHandler(Customer obj) {
        this.obj = obj;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            if (change) {
                System.out.print("real object : ");
                return method.invoke(obj, args);
            } else {
                System.out.print("invocation : ");
                return new Random().nextInt();
            }
        } finally {
            change = !change;
        }

    }
}
